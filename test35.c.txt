int main()
{
	FILE* pf = fopen("data.txt", "w");
	if (pf == NULL)
	{
		printf("%s\n", strerror(errno));
		return 0;
	}
	fputs("hello world\n", pf);
	fputs("hehe\n", pf);


	fclose(pf);
	pf = NULL;

	return 0;
}