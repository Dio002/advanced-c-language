#define PRINT(n) printf("the value of "#n" is %d\n", n)

int main()
{
	int a = 10;
	//printf("the value of a is %d\n", a);
	PRINT(a);
	printf("the value of ""a"" is %d\n", a);

	int b = 20;
	//printf("the value of b is %d\n", b);
	PRINT(b);
	printf("the value of ""b"" is %d\n", b);

	//printf("hello world\n");
	//printf("hello " "world\n");
	//printf("hel" "lo " "world\n");



	return 0;
}