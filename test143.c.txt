void MergeNopass(int* a, int n)
	{
		int* tem = (int*)malloc(sizeof(int) * n);
		int gap = 1;
		int left = 0;
		int right = 0;
		int left2 = 0;
		int right2 = 0;
		while (gap < n)
		{
			for (int i = 0; i < n; i += 2 * gap)
			{
				left = i;
				right = i + gap - 1;
				left2 = i + gap;
				right2 = i + 2 * gap - 1;

				if (right > n)//判断right越界
				{
					right = n - 1;
				}
				if (left2 >= n && right2 >= n)
				{
					left2 = n;
					right2 = n - 1;
				}
				if (right2 >= n && left2<n)
				{
					right2 = n - 1;
				}
				int count = i;
				printf("归并对象为【%d,%d】【%d,%d】\n", left, right, left2, right2);
				while (left <= right && left2 <= right2)
				{
					if (a[left] < a[left2])
					{
						tem[count++] = a[left++];
					}
					else
					{
						tem[count++] = a[left2++];
					}
				}
				while (left <= right)
				{
					tem[count++] = a[left++];
				}
				while (left2 <= right2)
				{
					tem[count++] = a[left2++];
				}
			}
			memcpy(a, tem, n * sizeof(int));
			gap *= 2;
		}
		free(tem);
	}//归并非递归
