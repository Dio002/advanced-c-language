int* selfDividingNumbers(int left, int right, int* returnSize){
    int* num = (int*)malloc(sizeof(int)*(right-left+1));
    *returnSize = 0;
    for(int i =left;i<=right;i++)
    {
        int k = 0;
        int a = i;
        while(a>0)
        {
          int b = a%10;
        if(b == 0 || i%b !=0)
        {
            k=1;
            break;
        }
        a/=10;
        }
         if(k==0)
        {
            num[(*returnSize)++]=i;
        }
    }
     return num;
}
   