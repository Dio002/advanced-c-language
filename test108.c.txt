struct ListNode {
    int val;
    struct ListNode *next;
    ListNode(int x) : val(x), next(NULL) {}
};*/
class PalindromeList {
public:
    bool chkPalindrome(ListNode* A) {
          if(A==NULL)
            return false;
        ListNode* slow=A,*fast=A;
        while(fast && fast->next){
            slow=slow->next;
            fast=fast->next->next;
        }
        ListNode* cur=NULL,*next=slow;
        while(slow){
            next=slow->next;
            slow->next=cur;
            cur=slow;
            slow=next;
        }
        next=A;
        while(cur){
            if(next->val!=cur->val)
                return false;
            next=next->next;
            cur=cur->next;
        }
        return true;
    }
};